# Hang Person

[_Archive.org_](https://www.archive.org/) Hangman Tutorial, reminiscent of my Java course in GUI Applications from [LFC](https://www.lakeforest.edu/academics/programs/math/).
 
* * *

## Requirements
This project makes use of **wxPython** to produce a graphical user interface (GUI). To install wxPython, you can:

1. Go to [wxPython.org](https://wxpython.org/) and navigate through the website to find the relevant download files.

2. Run the following code:

    `sudo apt-get install python-wxtools`

    Then,

    `sudo apt-get install python-wxgtk2.8`


3. Open Ubuntu Software Center, and search for **wxGlade**. This is a GUI designer similar to QT and Glade. This application is particularly useful because it will help you to create wxPython GUIs.
    apt-get install python-tk

* * *

## Run Application
Then navigate to your clone git repository directory, and run the application using the following code:

    python2.7 hangman.py


* * *

###Note

A message from the authors of *wxPython* - "_If you can't find what you're looking for then please consider figuring it out, and then write a recipe so others will not have to go through the same pain you did_". Thank you!


