#! /usr/bin/env python
from string import uppercase
import random as rand, wx, os
""" 
    COURTESY OF:
    ============
    1) Archive.Org
    2) ZetCode
"""

class HANGPERSON_GAME(wx.Frame):
  global name, version, solutions, solution, maxTries, guessed, guess, tryNum, letters, hangmanLettersFontSize, userInputLabelFontSize, hintTextFontSize, defaultGuessedLetters, guessedLettersFontSize, tryCountFontSize, debug, debugWord, imageDir, imageLocation, imageSelection, imageBaseName, imageExtension, imageLimit
  debug = True
  debugWord = "HANGMAN"
  name = "Hangman"
  version = "v1.0"
  solutions = []
  solution = ""
  letters = "X X X"
  maxTries = 0
  guessed = []
  guess = ""
  defaultGuessedLetters = "None"
  tryNum = 0
  hangmanLettersFontSize = 10
  userInputLabelFontSize = 8
  hintTextFontSize = 2
  guessedLettersFontSize = 5
  tryCountFontSize = 5
  imageLimit = 14
  imageSelection = "000"
  imageExtension = ".png"
  imageDir = "images"
  imageBaseName = "hangman"
  imageLocation = imageDir+os.sep+imageBaseName+str(imageSelection)+imageExtension

  def __init__(self, *args, **kw):
    super(HANGPERSON_GAME, self).__init__(*args, **kw)
    self.createWordList()
    self.makeMenuBar()
    self.CreateStatusBar()
    self.SetStatusText(name+" "+version)
    panel = wx.Panel(self)
    wrapper = wx.BoxSizer(wx.VERTICAL)
    topFrame = wx.BoxSizer(wx.HORIZONTAL)
    bottomFrame = wx.BoxSizer(wx.VERTICAL)

    self.hangmanImage = wx.StaticBitmap(panel, size=(-1,-1), bitmap=wx.Bitmap(imageLocation))
    self.hangmanLetters = wx.StaticText(panel, label=letters)
    fontHangmanLetters = self.hangmanLetters.GetFont()
    fontHangmanLetters.PointSize += hangmanLettersFontSize
    self.hangmanLetters.SetFont(fontHangmanLetters)
    topFrame.Add(self.hangmanImage, 0, wx.ALL | wx.SHAPED | wx.ALIGN_LEFT, 5)
    topFrame.AddSpacer((5,0))
    topFrame.Add(self.hangmanLetters, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

    userInput = wx.BoxSizer(wx.VERTICAL)
    userInputSub = wx.BoxSizer(wx.HORIZONTAL)   
    userInputLabel = wx.StaticText(panel, label="Guess a letter or guess the word:")
    fontUserInputLabel = userInputLabel.GetFont()
    fontUserInputLabel.PointSize += userInputLabelFontSize
    userInputLabel.SetFont(fontUserInputLabel)
    self.editUserInput = wx.TextCtrl(panel, size=wx.Size(100,-1))
    self.editUserInput.Bind(wx.EVT_KEY_UP, self.userResponse)
    userInputSub.AddMany(((userInputLabel, 0, wx.ALL | wx.ALIGN_LEFT, 5), (self.editUserInput, 0, wx.ALL | wx.ALIGN_RIGHT, 5)))
    hintText = wx.StaticText(panel, label="Use upper case letters ONLY.")
    fontHintText = hintText.GetFont()
    fontHintText.PointSize -= hintTextFontSize
    hintText.SetFont(fontHintText)
    userInput.AddMany(((userInputSub, 0, wx.ALIGN_TOP, 5), (hintText, 0, wx.ALIGN_BOTTOM, 5)))
    bottomFrame.Add(userInput, 0, wx.ALL, 10)
 
    guessedLettersSizer = wx.BoxSizer(wx.HORIZONTAL)
    guessedLettersLabel = wx.StaticText(panel, label="Guessed Letters:")
    fontGuessedLettersLabel = guessedLettersLabel.GetFont()
    fontGuessedLettersLabel.PointSize += guessedLettersFontSize
    guessedLettersLabel.SetFont(fontGuessedLettersLabel)
    self.guessedLetters = wx.StaticText(panel, label=defaultGuessedLetters)
    fontGuessedLetters = self.guessedLetters.GetFont()
    fontGuessedLetters.PointSize += guessedLettersFontSize
    self.guessedLetters.SetFont(fontGuessedLetters)
    guessedLettersSizer.AddMany(((guessedLettersLabel,0, wx.ALIGN_LEFT, 0),(self.guessedLetters, 0, wx.ALIGN_RIGHT, 0)))
    bottomFrame.Add(guessedLettersSizer, 0, wx.ALL, 10)

    tryCount = wx.BoxSizer(wx.HORIZONTAL)
    textTriesCountLabel = wx.StaticText(panel, label="Tries Left:")
    fontTextTryCountLabel = textTriesCountLabel.GetFont()
    fontTextTryCountLabel.PointSize += tryCountFontSize
    textTriesCountLabel.SetFont(fontTextTryCountLabel)
    self.textTriesCount  = wx.StaticText(panel, label=str(tryNum)+"/"+str(maxTries))
    fontTextTryCount = self.textTriesCount .GetFont()
    fontTextTryCount.PointSize += tryCountFontSize
    self.textTriesCount.SetFont(fontTextTryCount)
    tryCount.AddMany(((textTriesCountLabel, 0, wx.ALIGN_LEFT, 0),(self.textTriesCount , 0, wx.ALIGN_RIGHT, 0)))
    bottomFrame.Add(tryCount, 0, wx.ALL, 10)
 
    wrapper.Add(topFrame, 0, wx.ALIGN_TOP | wx.ALL, 10)  
    wrapper.Add(bottomFrame, 0, wx.ALIGN_BOTTOM, 0)
    self.newGame(None)
    panel.SetSizer(wrapper)
    wrapper.Fit(self)
  
    self.CenterOnScreen(dir=1)
    self.Show(True)
    return None

  def createWordList(self):
    global solutions
    _dir = os.getcwd()+os.sep+"words.txt"
    _file = open(_dir)
    try:
      for line in _file:
        word = line.strip().upper()
        if (word.isalpha()):
          solutions.append(word)
        else:
          pass
    except:
      print "Could not open the file you requested."
    finally:
      _file.close()
    
    return None

  def makeMenuBar(self):
    mainMenu = wx.Menu()
    aboutMenu = wx.Menu()
    startGame = mainMenu.Append(-1, "&New Game \tCtrl-N", "Create a new game")
    mainMenu.AppendSeparator()
    exitGame = mainMenu.Append(wx.ID_EXIT)
    aboutGame = aboutMenu.Append(wx.ID_ABOUT)
    menu = wx.MenuBar()
    menu.Append(mainMenu, "&File")
    menu.Append(aboutMenu, "&Help")
    self.SetMenuBar(menu)
    self.Bind(wx.EVT_MENU, self.newGame, startGame)
    self.Bind(wx.EVT_MENU, self.exitGame, exitGame)
    self.Bind(wx.EVT_MENU, self.aboutGame, aboutGame)
    return None
  
  def newGame(self, event):
    print "Starting a new game."
    self.initiateBoard()
    return None

  def aboutGame(self, event):
    wx.MessageBox(name+" "+version, name+" "+version, wx.OK|wx.ICON_INFORMATION)
    return None

  def exitGame(self, event):
    self.Close(True)
    print "Exiting "+name+". Thanks for playing."
    return None

  def initiateBoard(self):
   global solution, maxTries, guessed, tryNum, letters
   print "initiating the board"
   if debug:
    solution = debugWord
    pass
   else:
    solution = solutions[int((rand.random()*10)%len(solutions))]
    pass

   maxTries = len(solution)
   guessed = []
   tryNum = maxTries
   letters = "_"
   for dash in range(0,maxTries-1):
     letters += " _"
     continue
 
   self.updateInformation()
   return None

  def userResponse(self, event):
   global guess, guessed, tryNum, letters
   keyPressed = event.GetKeyCode()
   win = -1
   if keyPressed == wx.WXK_RETURN or keyPressed == wx.WXK_NUMPAD_ENTER:
     # check user input box
     guess = self.editUserInput.GetValue()
     guess = str(guess).strip()
     if guess.isalpha():
       if len(guess) > 1:
         tryNum = 0
         if (guess == solution) and guess.isupper():
           letters = ""
           for x in range(0,len(solution)):
             letters += solution[x]+" "
           win = 1
           pass
         else:
           win = 0
           pass
       elif len(guess) == 1:
         if guess in uppercase:
           tempLetters = ""
           if guess in solution:
             #no check if repeated letter
             test = ""
             for x in range(0,(len(solution)*2), 2):
               if solution[x/2] == guess:
                 tempLetters += guess+" "
                 test += guess
               else:
                 tempLetters += letters[x]+" "
                 test += letters[x]

             letters = tempLetters
             if test == solution:
               print "Win"
               win=1
             else:
               pass
           else:
             if guess in guessed:
               print "That letter was already used. Try a different letter."
               pass
             else:
               guessed += guess
               tryNum -= 1
               if tryNum == 0:
                 win = 0
               else:
                 print "Try again."
                 pass
         else:
           # guess is lowercase
           print "Invalid try use uppercase only."
           pass
       else:
         #length of guess less than 1
         pass
     else:
       #user input was not alphabetic
       pass

     self.updateInformation()
     self.editUserInput.Clear() 
     if win != -1:
       self.restartGamePrompt(win)
       pass
     else:
       pass 
   else:
    pass

   return None
  
  def updateInformation(self):
   global imageSelection
   self.hangmanLetters.SetLabel(label=letters)
   guessedLetters = ""
   for x in guessed:
     guessedLetters += x+" "
     pass
  
   self.guessedLetters.SetLabel(label=guessedLetters)
   self.textTriesCount.SetLabel(label=str(tryNum)+"/"+str(maxTries))
   imageNum = int((float(maxTries-tryNum)/float(maxTries))*imageLimit)

   if imageNum < 10:
     imageSelection = "00"+str(imageNum)
   else:
     imageSelection = "0"+str(imageNum)

   imageLocation = imageDir+os.sep+imageBaseName+str(imageSelection)+imageExtension
   self.hangmanImage.SetBitmap(bitmap=wx.Bitmap(imageLocation))
   return None

  def restartGamePrompt(self, Win):
   response = 0
   if Win == 1:
    response = wx.MessageBox("You Won! Do you want to play again?", "Winner", wx.YES_NO | wx.STAY_ON_TOP | wx.CENTRE)
   else:
    response = wx.MessageBox("Sorry that is not the word that I was looking for. Play again?", "Game Over", wx.YES_NO | wx.STAY_ON_TOP | wx.CENTRE)

   if response == wx.YES:
    self.newGame(wx.EVT_BUTTON)
   else:
    self.exitGame(wx.EVT_BUTTON)
  
  pass

app = wx.App()
frame = HANGPERSON_GAME(None, title="Hangman Game")
app.MainLoop()
